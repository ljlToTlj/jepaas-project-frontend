/*
 * @Descripttion: 
 * @Author: qinyonglian
 * @Date: 2020-05-29 10:08:38
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-01-26 13:59:43
 */
/**
 * 资源文件，app专用
 */
// eslint-disable-next-line func-names
(function () {
  // 开发模式
  let basicPath = '';
  let pluginPath = '';
  const scriptEls = document.getElementsByTagName('script');
  let include = '';
  for (let i = 0; i < scriptEls.length; i++) {
    const el = scriptEls[i];
    const src = el.getAttribute('src');
    if (src) {
      const index = src.indexOf('static/js/include.js');
      if (index != -1) {
        basicPath = src.substring(0, index);
        include = el.getAttribute('include');
        break;
      }
    }
  }
  // 开发环境
  const { href } = window.location;
  if (href.indexOf('/www/') != -1) {
    const www = href.split('/www/')[0];
    basicPath = `${www}/www/`;
    localStorage.setItem('www', basicPath);
  } else if (href.indexOf('/doc/') != -1) {
    const www = href.split('/doc/')[0];
    basicPath = `${www}/www/`;
    pluginPath = `${www}/doc/`;
    localStorage.setItem('www', basicPath);
  }
  const ux = {
    mui: ['static/ux/mui/mui.min.css', 'static/ux/mui/mui.js'],
    picker: ['static/ux/picker/mui.picker.all.css', 'static/ux/picker/mui.picker.all.js'],
    previewimage: ['static/ux/previewimage/mui.previewimage.css', 'static/ux/previewimage/mui.zoom.js', 'static/ux/previewimage/mui.previewimage.js'],
    ext: ['static/ux/ext/ext-debug.js'],
    resetcss: ['static/ux/flexible/css_reset.css'],
    flexible: ['static/ux/flexible/flexible.js'],
    vueRouter: ['static/ux/vue/vueRouter.js'],
    je: ['static/ux/je/je.min.css', 'static/ux/je/je.min.js'],
    waves: ['static/ux/waves/waves.min.js', 'static/ux/waves/waves.min.css'],
  };

  // 页面包含的组件
  let files = [];

  // 处理包含的组件
  if (include && include.length > 0) {
    const inclouds = include.split(',');
    inclouds.forEach((key) => {
      if (ux[key]) {
        files = files.concat(ux[key]);
      }
    });
  }

  // 公共样式
  files.push(
    'static/fonts/font-awesome/css/font-awesome.css',
    'static/fonts/jeicon/iconfont.css',
    'static/fonts/jeicon/iconfont.js',
    'static/ux/vue/vue.min.js',
    'static/ux/element/index.js',
    'static/ux/element/index.css',
  );
  // 注入到页面
  files.forEach((file) => {
    file = basicPath + file;
    if (file.indexOf('.js') != -1) {
      document.write(`<script type="text/javascript" src="${file}" charset="utf-8"></script>`);
    } else {
      document.write(`<link rel="stylesheet" type="text/css" href="${file}"/>`);
    }
  });
}());
