//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易
//             佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？

const HOST_BASIC = '';
const HOST_RBAC = '';
// 登录接口
export const POST_LOGIN = `${HOST_RBAC}/rbac/login/login`;
// 发送验证码
export const POST_SEND_VALIDATECODE = `${HOST_RBAC}/rbac/login/sendRandom`;
// 修改密码
export const POST_UPDATE_PWD = `${HOST_RBAC}/rbac/login/modifyPw`;
// export const POST_UPDATE_PWD = `${HOST_RBAC}/rbac/login/modifyPw`;
// 查询企业
export const GET_SEARCH_COMPANY = `${HOST_RBAC}/rbac/login/searchUserCompany`;
// 添加企业
export const POST_JOIN_COMPANY = `${HOST_RBAC}/je/app/AppUser/joinCompany`;
// 查询行业
export const GET_INDUSTRY = `${HOST_RBAC}/je/saas/saasYh/searchUsertrade`;
// 用户是否注册
export const POST_HAS_USER = `${HOST_RBAC}/rbac/login/validateRegister`;

// 检查用户
export const POST_CHECK_USER = `${HOST_RBAC}/rbac/login/checkUser`;

// 校验验证码
export const POST_CHECK_VALIDATECODE = `${HOST_RBAC}/rbac/login/validateRandom`;
// export const POST_CHECK_VALIDATECODE = `${HOST_RBAC}/rbac/login/validateRandom`;
// 注册绑定企业
export const POST_REGISTER_ACOOUNT = `${HOST_RBAC}/je/saas/saasYh/register`;
// 用户实体
export const POST_USER_CODE = `${HOST_RBAC}/rbac/login/getIdentityByUserCode`;
// export const POST_USER_CODE = `${HOST_RBAC}/je/login/getIdentityByUserCode`;

// app
// 发送状态机
export const POST_APP_CREATE_STATE = `${HOST_RBAC}/je/saas/saasYh/createState`;
// 第三方注册用户
export const POST_APP_REGISTER_USER = `${HOST_RBAC}/je/saas/saasYh/registerDsf`;
// 钉钉信息
export const POST_APP_DINGTALK_INFO = `${HOST_RBAC}/je/app/login/getDingTalkInfo`;

// 扫描确认登录
export const POST_SET_PHONE_QRCODE = `${HOST_RBAC}/je/app/login/setPhoneQRcode`;

/**
 *钉钉
 */
// 钉钉获取token
export const GET_DINGTALK_TOKEN = 'https://oapi.dingtalk.com/sns/gettoken';
// 钉钉获取token
export const GET_DINGTALK_PERSISTENT_CODE = 'https://oapi.dingtalk.com/sns/get_persistent_code';
// 钉钉获取token
export const GET_DINGTALK_SNS_TOKEN = 'https://oapi.dingtalk.com/sns/get_sns_token';
// 钉钉获取token
export const GET_DINGTALK_USERINFO = 'https://oapi.dingtalk.com/sns/getuserinfo';
