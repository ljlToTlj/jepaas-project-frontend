//  When I wrote this code, only God and I knew what it was. Now, only God knows!
/*
 * @Description:
 * @Author: yjp
 * @Date: 2020-11-20
 * @LastEditTime: 2020-11-20
 * @FilePath: /src/views/workflow/index.js
 */
import Vue from 'vue';
import install from '../../util/install';
import router from './router.js';
import Icon from '@/components/icon';
import config from './config.json';

Vue.component('Icon', Icon);
install(router, config);
