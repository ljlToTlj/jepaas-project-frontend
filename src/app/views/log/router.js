/*
 * @Descripttion: 
 * @Author: qinyonglian
 * @Date: 2021-02-07 10:15:24
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-02-07 10:33:24
 */
import createRouter from '../../util/RouterModel';
import index from './index.vue';
import detail from './view/detail.vue';
import list from './view/list.vue';

const trainRouter = [
  createRouter({
    path: '/JE-PLUGIN-LOG',
    name: 'index',
    component: index,
    children: [
      createRouter({
        path: '',
        name: 'list',
        meta: {
          white: true,
        },
        component: list,
      }),
      createRouter({
        path: 'detail',
        name: 'detail',
        meta: {
          white: true,
        },
        component: detail,
      }),
    ],
  }),
];
export default trainRouter;