/**
 * 自定义功能主控制器
 */
Ext.define("PRO.demo.controller.MainController", {
	extend: 'Ext.app.Controller',
	init:function(){
		//如果有多个控制器，可以在此加载
		//JE.initCtr('PRO.test.controller.Main1Controller');
		//JE.initCtr('PRO.test.controller.Main2Controller');
		//...;
		
		var self = this;
		var ctr={
			/**
			 * 绑定树形节点选中事件
			 */
			'panel[xtype=demo.westview]':{
				selectionchange:function(selM,sels){
					var mainview = selM.view.ownerCt.mainview,//主面板
						centerview = mainview.centerview;//数据面板
					//组装sql
					var sqls = {};
					Ext.each(sels,function(sel){
						if(sel.get('nodeType') != 'ROOT'){
							var value = sel.get('code'),
								field = sel.get('nodeInfo'),
								type = 'in';
							sqls[field] = sqls[field] || {code:field,type:type,value:[]};
							sqls[field].value.push(value);
						}
					});
					//拼接sql
					var sql = '';
					Ext.iterate(sqls,function(key,field){
						sql += ' AND '+JE.DB.buildWhereSql(field);
					});
					//设置sql
					JE.DB.setWhereSql(centerview.store,sql,'tree');
					//查询
					centerview.store.loadPage(1);
				}
			}
		}
		self.control(ctr);
	},
	views:[
		//加载引用的面板
		'PRO.demo.view.MainView',
		'PRO.demo.view.WestView',
		'PRO.demo.view.CenterView'
	]
});