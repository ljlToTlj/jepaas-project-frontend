const m = {
  keyword: '关键字',
  date: '日期',
  begin: '开始日期',
  end: '结束日期',
  search: '查询',
  screening: '筛选',
  unfinished: '未完结流程',
  finished: '已完结流程',
  canceldelay: '取消延迟',
  adddelay: '加入延迟',
  unmark: '取消标记',
  mark: '加入标记',
  form:"打开单据",
  classification: '流程分类',
};
export default m;
