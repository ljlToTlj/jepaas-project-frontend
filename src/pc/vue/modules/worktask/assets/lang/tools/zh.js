const m = {
  tijiaotongxunlu: '提交通讯录',
  dept: '部门',
  name: '姓名',
  search: '查询',
  unknown: '未知',
  tijiaobeiwanglu: '提交备忘录',
  save: '保 存',
  del: '删 除',
  notes: '笔记',
  addnotes: '添加笔记',
  tijiaoliucheng: '提交流程',
  log: '日志',
  memo: '备忘录',
  process: '流程',
  contact: '通讯录',
};

export default m;
