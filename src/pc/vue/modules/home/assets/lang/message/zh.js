const m = {
  bubble: '冒泡',
  allread: '全部设为已读',
  keyword: '关键字',
  date: '日期',
  begin: '开始日期',
  end: '结束日期',
  search: '查询',
  notice: '通知',
  notation: '批注',
  todayswork: '今日工作总结',
  weiwancheng: '未完成工作内容',
  this: '本',
  next: '下',
  jobsummary: '工作总结',
  gongzuojihua: '工作计划',
  gonzuoxinde: '工作心得总结',
  xiangguanfujian: '相关附件',
  zanwufujian: '暂无附件',
  noreading: '暂无已阅读',
  dianpingxinxi: '点评信息',
  read1: '已阅读',
  zanwudianpingxinxi: '暂无点评信息',
  close: '关闭',
  mingrijihua: '明日工作计划',
};
export default m;