#2.4.0APP插件开发指南(支持H5)

[TOC]
## 简介
JEPAAS开发平台移动端插件项目在上个版本中只支持APP插件开发，不支持在H5环境下使用开发，难以支持跨平台开发的业务，在新的插件项目中，插件的架构模式将由多页面应用修改为单页面应用，APP插件开发完毕后上传至JEPAAS平台处即可在H5、APP端同时使用！
## 开发
以p-suanbanyun-app项目为例，
1、在项目目录 src/views下新建插件目录；
2、在插件目录中新建插件的配置文件config.json
```javascript
	{
    "title":"插件中文名称",
    "include": [],
    "pluginName": "插件目录名称", // 注意，此处填写的插件目录名称必须与当前文件夹名称一致
    "pluginCode": "JE-PLUGIN-插件编码",
    "describe": "升级描述信息",
    "version": "1", // 插件版本，注意，平台导入插件时候会使用该版本信息
	"debug": true/false,
	"supportH5": true/false "是否支持H5模式"
	// 是否开启本地调试，如果开启本地调试后，
	// 在dev开发模式下会读取本地最新的插件代码，反之会读取云端的插件代码
}
```
3、在插件目录中新建index.js文件
```javascript

import router from './router'; // 此处的路由地址可自由修改
import config from './config.json';
import install from '../../util/install';
install(router, config);
```
4、在插件目录中新建router.js文件
```javascript
export default [
  {
    path: '/JE-PLUGIN-TEST', // 注意，一级路由必须为当前的插件编码
    name: 'JE-PLUGIN-TEST',
    component: rootPage,
    children: [
      {
        path: '/',
        component: Index,
      }
    ],
  }
];
```

5、在项目目录，config/目录下新建项目配置文件config.json
```javascript
	{
	  "entry": [
		"" // 要调试/打包的插件目录名称
	  ],
	  "username":"", // 平台用户名
	  "password":"", // 平台密码
	  "jeentry": [
		"je"
	  ],
	  "socket": false,
	  "proxyServerUrl": "", // 接口代理地址
	  "serverUrl": "" // H5目录地址
	}
```
### install方法的简介
```javascript
/**
 * 装载方法
 * @param routers 项目的路由信息
 * @param {pluginCode} 插件编码
 * @param {beforeInstall}(Vue) 在挂载到H5项目之前执行的方法
 */
function install(routers, { pluginCode }, { beforeInstall } = {}) {

}
```
## 首次发布
1、 进入JEPAAS快速开发平台，进去【开发】模块，选择JEAPP插件库

![](/docs/assets/images/img_1.png)

2、点击【导入插件】按钮，导入对应的插件

![](/docs/assets/images/img_2.png)

3、进入【JEAPP】菜单，选择对应的APP，点击【+】按钮，添加导入的插件
![](/docs/assets/images/img_3.png)
![](/docs/assets/images/img_4.png)
![](/docs/assets/images/img_5.png)
4、添加插件到首页菜单中
![](/docs/assets/images/img_6.png)
5、在【管理】菜单，角色权限下进行授权，即可完成首次发布
## 调试
#### H5调试
项目新建完毕后执行npm run dev即可启动服务，默认运行的端口为3000端口，通过http://localhost:3000即可进入项目
项目运行后会访问到云端的H5项目，如果插件配置文件（config.json）中debug选项为true，则访问对应的插件时会使用本地产生的最新的插件文件。
#### APP调试
通过Hbuilder开发工具，连接手机，运行项目后即可开启真机调试模式。
## 构建

   执行npm run build后，在appStatic文件夹下会产生可导入到JEPAAS平台的插件项目。

## 发布
### APP插件发布
第二次发布插件时可参考首次发布流程，进入JEAPP插件库，再次导入插件即可完成插件的发布工作。
### H5插件发布
将新的插件导入成功后，点击【发布】按钮，即可将最新的插件发布至所有使用该插件的APP下。
