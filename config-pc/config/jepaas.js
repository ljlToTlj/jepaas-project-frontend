/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-22 13:40:48
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-04-08 09:09:59
 */
module.exports = {
  remark: 'JEPAAS绿色版',
  cdn: '',
  proxyServerUrl: 'http://example.jepaas.com', // 代理服务器地址
  entry: ['home', 'login', 'im', 'demoboard', 'manage','operationlog','worktask'], // 打包的入口
  loginConfig: { // 登录配置
    TITLE: 'JEPaaS云平台', // 名称
    ICO: '/static/favicon.ico',
    LOGO: '', // logo
    THEMECOLOR: '', // 主题颜色
    BGIMG: '', // 登录背景图
    BGIMGBLUR: '', // 登录背景图-小图
    FINDPASSWORD: true, // 找回密码,
    FASTLOGIN: true, // 快捷登录
    OTHERLOGIN: { // 其他登录
      DTTALK: true, // 钉钉
      WECHAT: true, // 微信
      QQ: true, // QQ
      APP: true, // 轻云蒜瓣app
      PHONE: true, // 动态登录
    },
    ACCOUNT: [// 账户类型
      { type: 'USERNAME', enable: false, text: '用户名' }, // 允许用户名登录
      { type: 'ACCOUNT', enable: true, text: '账号' }, // 允许账号登录
      { type: 'PHONE', enable: true, text: '手机' }, // 允许手机登录
      { type: 'EMAILE', enable: false, text: '邮箱' }, // 允许邮箱登录
      { type: 'JOBNO', enable: false, text: '工号' }, // 允许工号登录
    ],
    SHOWI18N: true,
    I18NLIST: [
      { icon: 'jeicon-China', value: 'zh_CN', label: '简体中文' },
      { icon: 'jeicon-English', value: 'en', label: 'English' },
    ],
  },
};
